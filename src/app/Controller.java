package app;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.util.ArrayList;

public class Controller {
    @FXML Text messages;
    @FXML TextField messageInput;
    @FXML TextField nameInput;
    @FXML ScrollPane scrollPane;

    private ArrayList<Message> messagesList = new ArrayList<>();

    public void sendMessage() {
        String name = nameInput.getText();
        String message = messageInput.getText();

        if(name.equals("") || message.equals("")) {
            return;
        }

        messagesList.add(new Message(name, message));

        messageInput.setText("");
        refreshMessages();
    }

    private void refreshMessages() {
        String string = "";
        for(Message m : messagesList) {
            string = string + m.getFormatted();
        }

        messages.setText(string);
        scrollPane.setVvalue(1.0);
    }

    @FXML
    public void onEnter(ActionEvent ae){
        sendMessage();
    }
}
