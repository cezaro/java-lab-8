package app;

public class Message {
    public String name;
    public String message;

    public Message(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getFormatted() {
        return this.name + ": " + this.message + "\n";
    }
}
