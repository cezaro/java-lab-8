# Java lab 8

## Opis
Prosta aplikacja w JavaFX. Okienko chatu. Po wpisaniu imienia oraz wiadomości można je "wysyłać" do listy wiadomości.

## JavaWS
Przy pomocny IntelliJ można łatwo wygenerować plik \*.jar wraz z plikiem \*.jnlp. 
Należy wejść w **Project Structure > Artifacts**, dodać nowy **JavaFx Application**. Następnie w zakładce **Java FX** należy uzupełnić dane.

Po zbudowaniu pliku jar w pliku \*.jnlp należy dodać następujący parametr w drugiej linijce pliku:

`codebase="http://localhost:8080"`

Cała linijka powinna wyglądać następująco:

`<jnlp spec="1.0" xmlns:jfx="http://javafx.com" codebase="http://localhost:8080" href="java_lab_8.jnlp">`

Następnie należy wygenerować klucze oraz podpisać jar. Do generowania posłuży komenda:

`keytool -genkey -keystore lab8Keys -alias lab8`

Po wygenerowaniu klucza, podpisywanie jar:

`jarsigner -keystore lab8Keys java_lab_8.jar lab8`

Po tym wszystkim należy skopiować plik **\*.jar** oraz **\*.jnlp** do folderu Tomcata `/webapps/ROOT` i uruchomić serwer.

Wystarczy wejść pod link http://localhost:8080/java_lab_8.jnlp, pobrać plik i go uruchomić.

## Problemy z zabezpieczeniami Javy
Jeżeli wystąpi taki problem to należy wejśc w **Java Control Panel**, przejść do zakładki **Security**, a następnie dodać `http:/localhost:8080` do wyjątków. 